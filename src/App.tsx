import "./App.css";
import { useLayoutEffect, useMemo, useState } from "react";

function App() {
  const width = 200;
  const height = 200;
  const [countRock, setCountRock] = useState(0);
  const [countPaper, setCountPaper] = useState(0);
  const [countScissors, setCountScissors] = useState(0);
  const [countLizard, setCountLizard] = useState(0);
  const [countSpock, setCountSpock] = useState(0);

  // draw pixel to canvas
  const drawPixel = (
    ctx: CanvasRenderingContext2D,
    x: number,
    y: number,
    color: string,
  ) => {
    ctx.fillStyle = color;
    ctx.fillRect(x, y, 1, 1);
  };

  type PlayerType = "rock" | "paper" | "scissors" | "lizard" | "spock";

  interface Player {
    x: number;
    y: number;
    type: PlayerType;
  }

  const playerTypeColorMap: Record<PlayerType, string> = useMemo(
    () => ({
      rock: "black",
      paper: "grey",
      scissors: "hotpink",
      lizard: "green",
      spock: "blue",
    }),
    [],
  );

  const initPlayers = () => {
    const initializedPlayers: Player[] = [];
    const createPlayer = (type: PlayerType) => {
      const player: Player = {
        x: Math.floor(Math.random() * width),
        y: Math.floor(Math.random() * height),
        type,
      };
      initializedPlayers.push(player);
    };

    for (let i = 0; i < 500; i++) {
      createPlayer("rock");
      createPlayer("paper");
      createPlayer("scissors");
      createPlayer("lizard");
      createPlayer("spock");
    }
    return initializedPlayers;
  };

  const players: Player[] = initPlayers();

  useLayoutEffect(() => {
    const canvas = document.getElementById("battleArena") as HTMLCanvasElement;
    const ctx = canvas.getContext("2d") as CanvasRenderingContext2D;

    // set interval to draw pixel
    setInterval(() => {
      // clear canvas
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      players.forEach((player) => {
        drawPixel(ctx, player.x, player.y, playerTypeColorMap[player.type]);
      });

      // move player
      players.forEach((player) => {
        const move = Math.floor(Math.random() * 4);
        if (move === 0) {
          player.x++;
        } else if (move === 1) {
          player.x--;
        } else if (move === 2) {
          player.y++;
        } else {
          player.y--;
        }
        if (player.x >= width) {
          player.x = 0;
        }
        if (player.x < 0) {
          player.x = width - 1;
        }
        if (player.y >= height) {
          player.y = 0;
        }
        if (player.y < 0) {
          player.y = height - 1;
        }
      });

      const rules = {
        rock: ["scissors", "lizard"],
        paper: ["rock", "spock"],
        scissors: ["paper", "lizard"],
        lizard: ["paper", "spock"],
        spock: ["rock", "scissors"],
      };

      // check for collision
      players.forEach((player) => {
        players.forEach((otherPlayer) => {
          if (
            player.x === otherPlayer.x &&
            player.y === otherPlayer.y &&
            player.type !== otherPlayer.type
          ) {
            if (rules[player.type].includes(otherPlayer.type)) {
              otherPlayer.type = player.type;
            } else {
              player.type = otherPlayer.type;
            }
          }
        });
      });

      setCountRock(players.filter((player) => player.type === "rock").length);
      setCountPaper(players.filter((player) => player.type === "paper").length);
      setCountScissors(
        players.filter((player) => player.type === "scissors").length,
      );
      setCountLizard(
        players.filter((player) => player.type === "lizard").length,
      );
      setCountSpock(players.filter((player) => player.type === "spock").length);
    }, 100);
  }, []);

  return (
    <>
      <p>Rock Paper Scissors Lizard Spock Simulator</p>
      <div style={{ display: "flex" }}>
        <div>
          <canvas id={"battleArena"} width="{width}" height="{height}" />
        </div>
        <div>
          Color key:
          <div style={{ color: playerTypeColorMap["rock"] }}>
            Rock {countRock}
          </div>
          <div style={{ color: playerTypeColorMap["paper"] }}>
            Paper {countPaper}
          </div>
          <div style={{ color: playerTypeColorMap["scissors"] }}>
            Scissors {countScissors}
          </div>
          <div style={{ color: playerTypeColorMap["lizard"] }}>
            Lizard {countLizard}
          </div>
          <div style={{ color: playerTypeColorMap["spock"] }}>
            Spock {countSpock}
          </div>
        </div>
      </div>
    </>
  );
}

export default App;
