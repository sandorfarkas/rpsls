# Rock Paper Scissors Lizard Spock Simulator

## Description

This is a simulator for the game Rock Paper Scissors Lizard Spock. It is a variation of the classic game Rock Paper Scissors, created by Sam Kass and Karen Bryla. The game was popularized by the TV show The Big Bang Theory.

## Run in your browser

[Click here to open the simulator](https://sandorfarkas.gitlab.io/rpsls/)